import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { concatAll, concatMap, mergeAll, mergeMap, toArray } from 'rxjs/operators';

const apiUrl = 'http://localhost:7733/api/users';

@Component({
  selector: 'app-test-merge-map',
  templateUrl: './test-merge-map.component.html',
  styleUrls: ['./test-merge-map.component.scss']
})
export class TestMergeMapComponent implements OnInit {

  users: Observable<Array<User>>;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    const singleUsers = this.http.get<User[]>(apiUrl)
      .pipe(
        concatAll()   // flatten nexted arrays, so concatMap can process each item
      ) as Observable<User>;  // bug in concatAll?

    this.users = singleUsers.pipe(
      mergeMap(u => this.getUserDetails(u.moniker)),
      toArray()
    );

    // this works, but displays typings error
    // this.users = this.http.get<User[]>(apiUrl)
    //   .pipe(
    //     concatAll(),
    //     concatMap(u => this.getUserDetails(u.moniker)),
    //     toArray()
    //   );
  }

  getUserDetails(moniker: string): Observable<User> {
    return this.http.get<User>(`${apiUrl}/${moniker}`)
      .pipe(

      );
  }

}

export interface User {
  moniker: string;
  firstName: string;
  lastName: string;
  kurzZeichen: string;
  sektion: string;
  email: string;
}
