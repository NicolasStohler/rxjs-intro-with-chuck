import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestMergeMapComponent } from './test-merge-map.component';

describe('TestMergeMapComponent', () => {
  let component: TestMergeMapComponent;
  let fixture: ComponentFixture<TestMergeMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestMergeMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestMergeMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
