export interface User {
  moniker: string;
  firstName: string;
  lastName: string;
  kurzZeichen: string;
  sektion: string;
  email: string;
}
