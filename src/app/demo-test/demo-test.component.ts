import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../User';

const apiUrl = 'http://localhost:7733/api/users';

@Component({
  selector: 'app-demo-test',
  templateUrl: './demo-test.component.html',
  styleUrls: ['./demo-test.component.scss']
})
export class DemoTestComponent implements OnInit {

  title = 'Das ist der Demo test';
  users = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get<User[]>(apiUrl)
      .subscribe(data => this.users = data);
  }

  onClick() {
    this.users.push(this.users.length + 1);
  }

}


