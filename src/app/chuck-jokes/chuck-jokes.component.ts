import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject ,  timer ,  Observable ,  merge ,  Subject } from 'rxjs';
import { Joke, JokeResponse } from '../shared/joke';
import { concatMap, map, publish, share, shareReplay, skipWhile, switchMap, takeUntil, tap } from 'rxjs/operators';

const API_ENDPOINT = 'https://api.icndb.com/jokes';
// const API_ENDPOINT = 'https://api.icndb.com/jokes/random/5?limitTo=[nerdy]&escape=javascript';
const REFRESH_INTERVAL = 3000;

@Component({
  selector: 'app-chuck-jokes',
  templateUrl: './chuck-jokes.component.html',
  styleUrls: ['./chuck-jokes.component.scss']
})
export class ChuckJokesComponent implements OnInit {
  // demoing this:
  // https://blog.strongbrew.io/rxjs-polling/

  polledJoke$: Observable<Array<Joke>>;
  load$ = new BehaviorSubject<string>('');
  nerdyFlag$ = new BehaviorSubject<boolean>(true);
  pauseFeed$ = new BehaviorSubject<boolean>(false);
  pauseSubject$ = new Subject<void>();

  constructor(private http: HttpClient) { }

  ngOnInit() {
    // read jokes like here:
    // https://blog.thoughtram.io/angular/2018/03/05/advanced-caching-with-rxjs.html
    // https://stackblitz.com/edit/advanced-caching-with-rxjs-step-1?file=app%2Fjoke.service.ts

    const timer$ = timer(0, REFRESH_INTERVAL).pipe(
      concatMap(() => this.fetchJokes()),
      takeUntil(this.pauseSubject$),              // => stops the polling
    );

    this.polledJoke$ = merge(this.load$, this.nerdyFlag$, this.pauseFeed$).pipe(
      switchMap(() => timer$ ),
      share()   // WARNING: shareReplay(1) results in timer continuing even when destroying component
    );
  }

  refreshDataClick() {
    this.load$.next('');
  }

  toggleNerdy() {
    this.nerdyFlag$.next(!this.nerdyFlag$.getValue());
  }

  pauseFeed() {
    this.pauseFeed$.next(!this.pauseFeed$.getValue());

    if (this.pauseFeed$.getValue()) {
      this.pauseSubject$.next();
    } else {
      this.pauseSubject$ = new Subject<void>();
    }
  }

  randomIntFromInterval(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  private fetchJokes(): Observable<Array<Joke>> {
    return this.http.get<JokeResponse>(this.getApiEndpoint()).pipe(
      map(response => response.value)
    );
  }

  private getApiEndpoint(): string {
    // console.log('nerdy:', this.nerdyFlag$.getValue());
    const nerdyFilter = (this.nerdyFlag$.getValue()) ? '&limitTo=[nerdy]' : '';
    const jokeCount = this.randomIntFromInterval(3, 6);

    return `${API_ENDPOINT}/random/${jokeCount}?escape=javascript${nerdyFilter}`;
  }

}
