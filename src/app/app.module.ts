import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { SimpleCounterComponent } from './simple-counter/simple-counter.component';
import { TestMergeMapComponent } from './test-merge-map/test-merge-map.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { DemoTestComponent } from './demo-test/demo-test.component';
import { UserItemComponent } from './user-item/user-item.component';
import { JokeComponent } from './joke/joke.component';
import { ChuckJokesComponent } from './chuck-jokes/chuck-jokes.component';


@NgModule({
  declarations: [
    AppComponent,
    SimpleCounterComponent,
    TestMergeMapComponent,
    DemoTestComponent,
    UserItemComponent,
    JokeComponent,
    ChuckJokesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
