import { RouterModule, Routes } from '@angular/router';
import { SimpleCounterComponent } from './simple-counter/simple-counter.component';
import { TestMergeMapComponent } from './test-merge-map/test-merge-map.component';
import { NgModule } from '@angular/core';
import { DemoTestComponent } from './demo-test/demo-test.component';
import { JokeComponent } from './joke/joke.component';
import { ChuckJokesComponent } from './chuck-jokes/chuck-jokes.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'simple-counter', pathMatch: 'full' },
  {
    path: 'simple-counter',
    component: SimpleCounterComponent
  },
  {
    path: 'test-merge-map',
    component: TestMergeMapComponent
  },
  {
    path: 'demo-test',
    component: DemoTestComponent
  },
  {
    path: 'joke',
    component: JokeComponent
  },
  {
    path: 'chuck',
    component: ChuckJokesComponent
  },
  { path: '**', component: SimpleCounterComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
