import { Component, OnInit } from '@angular/core';
import { timer ,  Observable ,  of ,  Subscription ,  Subject } from 'rxjs';
import { buffer, bufferCount, bufferTime, concatMap, filter, map, scan, share, takeUntil, toArray } from 'rxjs/operators';

@Component({
  selector: 'app-simple-counter',
  templateUrl: './simple-counter.component.html',
  styleUrls: ['./simple-counter.component.scss']
})
export class SimpleCounterComponent implements OnInit {

  allValues: Observable<Array<number>>;
  evenValues: Observable<Array<number>>;

  myTimer: Observable<number>;
  mySubject = new Subject<void>();

  constructor() { }

  ngOnInit() {
  }

  start() {
    this.myTimer = timer(0, 1000)
      .pipe(
        takeUntil(this.mySubject),
        map(x => +x)
      );

    this.allValues = this.myTimer.pipe(
      // concatMap(value => of(value)),
      scan((acc: Array<number>, value: number) => {
        acc.push(value);
        return acc;
      }, []),
      share()
    );

    const logger = this.allValues.subscribe(x => console.log(x));

    this.evenValues = this.myTimer.pipe(
      filter(x => x % 2 === 0),
      scan((acc: Array<number>, value: number) => {
        acc.push(value);
        return acc;
      }, [])
    );
  }

  stop() {
    this.mySubject.next();
    this.myTimer = null;
  }

}
