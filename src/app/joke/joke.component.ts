import { Component, OnInit } from '@angular/core';
import { Subject ,  BehaviorSubject ,  combineLatest,  Observable ,  timer,  merge } from 'rxjs';
import { map, startWith, tap } from 'rxjs/operators';

@Component({
  selector: 'app-joke',
  templateUrl: './joke.component.html',
  styleUrls: ['./joke.component.scss']
})
export class JokeComponent implements OnInit {

  // simpleCommand$ = new BehaviorSubject<string>('');
  simpleCommand$ = new Subject<void>();
  toggleCommand$ = new BehaviorSubject<boolean>(true);
  timer$: Observable<number>;

  display$: Observable<string>;

  constructor() { }

  ngOnInit() {
    // react on two buttons, timer

    const simple$ = this.simpleCommand$.pipe(
      tap((data) => console.log(`simpleCommand$ ${data}`)),
    );

    // simple$.subscribe(() => {
    //
    // });

    const toggle$ = this.toggleCommand$.pipe(
      tap((data) => console.log(`toggleCommand$ ${data}`)),
    );

    this.timer$ = timer(0, 1000).pipe(
      tap((data) => console.log(`timer$ ${data}`))
    );

    // merge         => emits when any of the streams emits
    // combineLatest => only emits after all streams have emited at least once!

    // Advantage: possible to access emited values of combined streams
    // const combined$ = combineLatest(
    //   simple$, toggle$, this.timer$,
    //   (simple, toggle) => {
    //     console.log(`COMBINED simple/toggle: ${simple} - ${toggle}`);
    //   });

    // Note: access to emited value of stream that emited only
    const merged$ = merge(simple$, toggle$, this.timer$).pipe(
      tap((data) => console.log(`MERGED simple/toggle: ${data}`)),
      map((data) => `MERGED simple/toggle: ${data}`)
    );

    this.display$ = merged$;

    // combined$.subscribe((data) => {
    //   return `combined$.subscribe: ${data}`;
    // });

  }

  onSimpleCommand() {
    // this.simpleCommand$.next('click');  // for BehaviorSubject
    this.simpleCommand$.next();
  }

  onToggleCommand() {
    this.toggleCommand$.next(!this.toggleCommand$.getValue());
  }
}
